#ifndef STACKTEMP_H
#define STACKTEMP_H


template <class T>
class Stack
{
    public:
    Stack()=default;
    void push(T i);
    T pop();
    void print();
    ~Stack();
private:
    struct StackElem
    {
            T _value;
            struct StackElem* _next = nullptr;
    };

    StackElem* _first = nullptr;
    StackElem* _last = nullptr;
    int _elem_num = 0;
};



#include "stacktemp.tpp"

#endif // STACKTEMP_H
