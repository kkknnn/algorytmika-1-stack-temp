#include "stacktemp.h"
#include <iostream>


template<class T>
void Stack<T>::push(T i)
{
    StackElem * Node = new StackElem;
    Node->_value=i;

    if(_first==nullptr)
    {
        Node->_next=Node;
    }
    else
    {
    Node->_next=_first;
    }

    _elem_num++;

    _first=Node;

    if (nullptr==_last)
    {

        _last=Node;
    }

    return;

}

template<class T>
T Stack<T>::pop()
{


    if (_elem_num!=0)
    {
        _elem_num--;
        T element_to_return = _first->_value;
        auto Temp = _first->_next;
        delete _first;
        _first = Temp;
        if (_elem_num==0)

        {   _first=nullptr;
            _last=nullptr;
        }
        return element_to_return;
    }

    else
    {
        std::cout <<"brak elementow na stosie" << "\n";
    }

}

template<class T>
void Stack<T>::print()
{
    if (_elem_num!=0)
    {
        auto iterator = _first;
       while (true)
       {
           auto temp = iterator->_next;
           std::cout << "Elem " << iterator->_value<< '\n';
           if (iterator==_last)
           {
               break;
           }
           iterator=temp;


       }
    }
    else
    {
        std::cout <<"brak elementow na stosie"<< "\n";
    }
}

template<class T>
Stack<T>::~Stack()
{

    if (_elem_num>0)
    {
        auto iterator = _first;

        while(true)
      {
            auto temp=iterator->_next;

            delete iterator;

            _elem_num--;

            if (0 == _elem_num)
            {
                break;
            }
            iterator = temp;

     }
    }

}
