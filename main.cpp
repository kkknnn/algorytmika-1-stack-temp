#include <iostream>
#include "stacktemp.h"

using namespace std;

int main()
{

    Stack <int> _stack;
    _stack.push(30);
    _stack.push(40);
    _stack.pop();
    _stack.push(40);
    _stack.print();

    return 0;
}
